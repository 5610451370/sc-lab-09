package Model;

import Interface.Taxable;

public class ProductTaxable implements Taxable , Comparable<ProductTaxable>{
	
	private String productName;
	private double price;
	
	public ProductTaxable(String proName, double price) {
		this.productName = proName;
		this.price = price;
	}

	public String getProName(){
		return productName;
	}
	
	@Override
	public double getTax() {
		return price*0.07;
	}

	@Override
	public int compareTo(ProductTaxable other) {
		if (this.price < other.price ) { return -1; }
		if (this.price > other.price ) { return 1;  }
		return 0;
	}
	
	public String toString(){
		return "Produt : "+productName+"  Price : "+price;
	}

}
