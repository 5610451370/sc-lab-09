package Model;

import java.util.Comparator;


public class ExpenseComparator implements Comparator<CompanyTaxable> {

	@Override
	public int compare(CompanyTaxable o1, CompanyTaxable o2) {
		double expense1 = o1.getDisbursement();
		double expense2 = o2.getDisbursement();
		if (expense1 > expense2) return 1;
		if (expense1 < expense2) return -1;
		return 0;
	}

}
