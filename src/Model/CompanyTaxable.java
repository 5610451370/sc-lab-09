package Model;

import Interface.Taxable;

public class CompanyTaxable implements Taxable {
	
	private String companyName;
	private double receipts;
	private double disbursement;

	public CompanyTaxable(String comName, double receipts, double disbursement) {
		this.companyName = comName;
		this.receipts = receipts;
		this.disbursement = disbursement;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	
	@Override
	public double getTax() {
		return ((receipts-disbursement)*0.3);
	}

	public double getProfit() {
		return (receipts-disbursement);
	}
	
	public double getReceipts(){
		return receipts;
	}
	
	public double getDisbursement(){
		return disbursement;
	}
	
	public String toString(){
		return "Name : "+companyName+"  Earning : "+receipts+"  Expense : "+disbursement;
	}
}
