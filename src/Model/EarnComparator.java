package Model;


import java.util.Comparator;


public class EarnComparator implements Comparator<CompanyTaxable> {

	@Override
	public int compare(CompanyTaxable o1, CompanyTaxable o2) {
		double earn01 = o1.getReceipts();
		double earn02 = o2.getReceipts();
		if (earn01 > earn02) return 1;
		if (earn01 < earn02) return -1;
		
		return 0;
	}

}
