package Model;

import java.util.Comparator;


public class ProfitComparator implements Comparator<CompanyTaxable> {

	@Override
	public int compare(CompanyTaxable o1, CompanyTaxable o2) {
		double pro01 = o1.getProfit();
		double pro02 = o2.getProfit();
		if (pro01 > pro02) return 1;
		if (pro01 < pro02) return -1;
		return 0;
	}

}
