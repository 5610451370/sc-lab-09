package Model;

import Interface.Taxable;

public class PersonTaxable implements Taxable , Comparable<PersonTaxable>{
	
	private String name;
	private double salary;
	
	public PersonTaxable(String name, double salary) {
		this.name = name;
		this.salary = salary;
	}
	
	public String getName() {
		return name;
	}

	@Override
	public double getTax() {
		
		if (salary <=300000) return salary*0.05;
		
		else return (300000*0.05)+((salary-300000)*0.1);
		
	}

	@Override
	public int compareTo(PersonTaxable other) {
		if (this.salary < other.salary ){
			return -1;
			}
		if (this.salary > other.salary ) {
			return 1;
			}
		return 0;
	}
	
	public String toString(){
		return "Name : "+name+"  SalaryYear : "+salary;
	}

}
