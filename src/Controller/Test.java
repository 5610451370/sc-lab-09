package Controller;

import java.util.ArrayList;
import java.util.Collections;

import Interface.Taxable;
import Model.CompanyTaxable;
import Model.EarnComparator;
import Model.ExpenseComparator;
import Model.PersonTaxable;
import Model.ProductTaxable;
import Model.ProfitComparator;
import Model.TaxComparator;



public class Test {
	
	public static void main(String[] args) {
		Test test = new Test();
		test.testCase();
	}
	
	public void testCase() {
		
		System.out.println("Test Comparable Person");
		ArrayList<PersonTaxable> personarray = new ArrayList<PersonTaxable>();
		personarray.add(new PersonTaxable("Robert",600000));
		personarray.add(new PersonTaxable("Robinhood",300000));
		personarray.add(new PersonTaxable("Mark",500200));
		personarray.add(new PersonTaxable("To",555000));
		
		System.out.println("Before");
		
		for (PersonTaxable a : personarray) {
			System.out.println(a);
		}
		
		Collections.sort(personarray);
		System.out.println("\nAfter sort with Earning");
		for (PersonTaxable b : personarray) {
			System.out.println(b);
		}
		System.out.println("===============================================================");

		
		System.out.println("Test Comparable Product");
		ArrayList<ProductTaxable> proarray = new ArrayList<ProductTaxable>();
		proarray.add(new ProductTaxable("Notebook",40000));
		proarray.add(new ProductTaxable("Mobile Phone",10000));
		proarray.add(new ProductTaxable("Personal Computer",50000));
		proarray.add(new ProductTaxable("Bed",2000));
		System.out.println("Before");
		for (ProductTaxable a : proarray) {
			System.out.println(a);
		}
		
		Collections.sort(proarray);
		System.out.println("\nAfter sort with Price");
		for (ProductTaxable b : proarray) {
			System.out.println(b);
		}
		System.out.println("===============================================================");
		
		
		System.out.println("Test Comparator Company");
		ArrayList<CompanyTaxable> comarray = new ArrayList<CompanyTaxable>();
		comarray.add(new CompanyTaxable("Sun",1000,10000));
		comarray.add(new CompanyTaxable("Hugo",40000,7000));
		comarray.add(new CompanyTaxable("IBM",30,20));
		comarray.add(new CompanyTaxable("AIS",500,200));
		
		System.out.println("Before");
		for (CompanyTaxable a : comarray) {
			System.out.println(a);
		}
		
		Collections.sort(comarray, new EarnComparator());
		System.out.println("\nAfter sort with Earning");
		for (CompanyTaxable b : comarray) {
			System.out.println(b);
		}
		
		Collections.sort(comarray, new ExpenseComparator());
		System.out.println("\nAfter sort with Expense");
		for (CompanyTaxable c : comarray) {
			System.out.println(c);
		}
		
		Collections.sort(comarray, new ProfitComparator());
		System.out.println("\nAfter sort with Profit");
		for (CompanyTaxable d : comarray) {
			System.out.println(d);
		}
		System.out.println("===============================================================");

		
		System.out.println("Test Comparable Taxable");
		ArrayList<Taxable> taxarray = new ArrayList<Taxable>();
		taxarray.add(new PersonTaxable("Suzee",70000));
		taxarray.add(new PersonTaxable("Gerard",20000));
		taxarray.add(new ProductTaxable("Notebook",48790));
		taxarray.add(new ProductTaxable("Shirt",500));
		taxarray.add(new CompanyTaxable("AIS",864811,300000));
		taxarray.add(new CompanyTaxable("TRUE",770000,600000));

		System.out.println("Before");
		for (Taxable c : taxarray) {
			System.out.println(c);
		}
		
		Collections.sort(taxarray, new TaxComparator());
		System.out.println("\nAfter sort with Tax");
		for (Taxable c : taxarray) {
			System.out.println(c);
		}
	}


}
